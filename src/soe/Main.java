package soe;

import soe.bowling.lectura.ImpImpresion;
import soe.bowling.lectura.ImpLectura;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import soe.bowling.ImpPuntajeJuego;
import soe.bowling.IPuntajeJuego;
import soe.bowling.lectura.ILectura;
import soe.bowling.lectura.IImpresion;

public class Main {

  public static Logger log = LogManager.getLogger();

  public static void main(String[] args) {
    try {
      String archivo="";
    if (args.length <= 0) {
     archivo ="test.txt";
    }
    else
        archivo=  args[0];
      File file=new File(archivo);
      ILectura dataReader = new ImpLectura(new Scanner(file));

      IPuntajeJuego dataManager = new ImpPuntajeJuego(dataReader.readPlayerGames());
      dataManager.procesar();

      IImpresion outputPrinter = new ImpImpresion();
      outputPrinter.print(dataManager);
    } catch (FileNotFoundException e) {
      log.error(e.getMessage());
      System.exit(-1);
    } finally {
      System.exit(0);
    }
  }
}
