package soe.bowling.lectura;

import soe.bowling.errores.PuntosException;
import soe.bowling.Constantes;
import soe.bowling.entidades.Jugador;
import soe.bowling.entidades.Turno;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import soe.bowling.IPuntaje;
import soe.bowling.IPuntajeJuego;

public class ImpImpresion implements IImpresion {

  private final IPuntaje scoreParser = IPuntaje.defaultParser();

  @Override
  public void print(IPuntajeJuego scoreDataManager) {
    System.out.println(
        String.join(
            "\n",
            this.juegoRowToPrint(),
            scoreDataManager.getPlayerGames()
                .stream()
                .sorted((pg1, pg2) -> Boolean.compare(pg2.isValido(), pg1.isValido()))
                .map(this::printablePlayerGame)
                .collect(Collectors.joining("\n"))));
  }

  private String juegoRowToPrint() {
    String headerRow = "juego";
    for (int i = 1; i <= Constantes.JUEGOS_LENGTH; i++) {
      headerRow = headerRow.concat(String.format("\t\t%d", i));
    }
    return headerRow;
  }

  private String printablePlayerGame(Turno playerGame) {
    if (!playerGame.isValido()) {
      return String.format("[%s does not have a valid game]", playerGame.getNombre());
    }

    final List<Jugador> juegos = playerGame.getjuegos();
    return String.join(
        "\n", playerGame.getNombre(), this.pinfallsRowToPrint(juegos), this.scoresRowToPrint(juegos));
  }

  private String pinfallsRowToPrint(List<Jugador> juegos) {
    String rowName = "Pinfalls";
    String juegoPoints =
        juegos
            .stream()
            .map(f -> {
                  try {
                    return this.pointsToPrint(f);
                  } catch (PuntosException e) {
                    return "Invalid points detected";
                  }
                })
            .collect(Collectors.joining(""));

    return String.join("\t", rowName, juegoPoints);
  }

  private String scoresRowToPrint(List<Jugador> juegos) {
    String rowName = "Score";
    String juegoScores =
        juegos
            .stream()
            .map(f -> String.format("%d", f.getPunto()))
            .collect(Collectors.joining("\t\t"));

    return String.join("\t\t", rowName, juegoScores);
  }

  public String pointsToPrint(Jugador juego) throws PuntosException {
    if (juego.getPuntosStr().length == 1
        && this.scoreParser.parseIntPoints(juego.getPuntosStr()[0]) == Constantes.STRIKE_VALUE) {
      return String.format(juego.getIndex() == 1 ? "\t%s" : "\t\t%s", Constantes.CHUZA);
    }

    if (juego.getPuntosStr().length == 2 && juego.sumOfPoints() == Constantes.MAX_PUNTOS) {
      return String.format(juego.getIndex() == 1 ? "%s\t%s" : "\t%s\t%s", juego.getPuntosStr()[0], Constantes.SPARE);
    }

    List<String> maskValues = new ArrayList<>();

    for (String stringPoint : juego.getPuntosStr()) {
      maskValues.add(this.scoreParser.parseIntPoints(stringPoint) == Constantes.MAX_PUNTOS ? Constantes.CHUZA : stringPoint);
    }

    String pointsAsString = maskValues.stream().collect(Collectors.joining("\t"));
    return juego.isFirstjuego() ? pointsAsString : "\t".concat(pointsAsString);
  }
}
