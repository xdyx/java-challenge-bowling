package soe.bowling.lectura;

import soe.bowling.IPuntajeJuego;

public interface IImpresion {

  void print(IPuntajeJuego scoreDataCenter);
}
