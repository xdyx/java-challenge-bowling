package soe.bowling.lectura;

import soe.bowling.entidades.Turno;
import java.util.List;

public interface ILectura {

  List<Turno> readPlayerGames();
}
