package soe.bowling.lectura;

import soe.bowling.entidades.Turno;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ImpLectura implements ILectura {

  public static final Logger log = LogManager.getLogger();

  private List<Turno> playerGames;
  private Scanner scanner;

  public ImpLectura(Scanner scanner) {
    this.playerGames = new ArrayList<>();
    this.scanner = scanner;
  }

  @Override
  public List<Turno> readPlayerGames() {
    while (this.scanner.hasNext()) {
      this.processLine(this.scanner.nextLine());
    }
    return playerGames;
  }

  public void processLine(String nextLine) {
    if (Objects.isNull(nextLine) || nextLine.isEmpty()) {
      log.info("Empty ROW skipped from input data text file");
      return;
    }

    String sanitized =
        nextLine
            .trim()
            .toUpperCase()
            .replaceAll("\\t", " ") // Replace tabs with space
            .replaceAll("(\\s)+", " "); // Normalize spaces

    String[] values = sanitized.split(" ");
    if (values.length < 1 || values.length > 2) {
      log.info(String.format("Invalid row input: [%s] cannot processed", nextLine));
      System.exit(-1);
    }
    this.addOrUpdate(values[0], values[1]);
  }

  private void addOrUpdate(String playerName, String inputPoint) {
    Turno playerGame =
        this.findByPlayerName(playerName)
            .orElseGet(() -> {
                  Turno newPlayerGame = new Turno(playerName);
                  this.playerGames.add(newPlayerGame);
                  return newPlayerGame;
                });
    playerGame.getPuntos().add(inputPoint);
  }

  private Optional<Turno> findByPlayerName(String playerName) {
    return this.playerGames.stream().filter(pg -> playerName.equals(pg.getNombre())).findFirst();
  }
}
