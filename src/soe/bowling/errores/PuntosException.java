package soe.bowling.errores;

public class PuntosException extends Exception {

  public PuntosException(String message) {
    super(message);
  }
}
