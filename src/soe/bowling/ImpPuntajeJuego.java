package soe.bowling;

import soe.bowling.entidades.Turno;
import java.util.ArrayList;
import java.util.List;

public class ImpPuntajeJuego implements IPuntajeJuego {

  private List<Turno> playerGames;
  private IPuntajeCalculadora scoreCalculator;

  public ImpPuntajeJuego() {
    this(new ArrayList<>(), IPuntajeCalculadora.defaultScoreCalculator());
  }

  public ImpPuntajeJuego(List<Turno> playerGames) {
    this(playerGames, IPuntajeCalculadora.defaultScoreCalculator());
  }

  public ImpPuntajeJuego(List<Turno> playerGames, IPuntajeCalculadora scoreCalculator) {
    this.playerGames = playerGames;
    this.scoreCalculator = scoreCalculator;
  }

  @Override
  public List<Turno> getPlayerGames() {
    return this.playerGames;
  }

  @Override
  public void procesar() {
    for (Turno playerGame : this.playerGames) {
      this.scoreCalculator.calcularPuntos(playerGame);
    }
  }
}
