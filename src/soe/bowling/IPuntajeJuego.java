package soe.bowling;

import soe.bowling.entidades.Turno;
import java.util.List;

public interface IPuntajeJuego {

  List<Turno> getPlayerGames();
  void procesar();
}
