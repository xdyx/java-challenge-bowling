package soe.bowling.estrategias;

import soe.bowling.Constantes;
import soe.bowling.entidades.Jugador;
import soe.bowling.IEstrategia;

public interface EstrategiasProvider {

  static IEstrategia provideFor(Jugador juego) {
    if (juego.getPuntos().length == 1 && juego.sumOfPoints() == Constantes.STRIKE_VALUE) {
      return new ImpStrategiaStrike();
    }

    if (juego.getPuntos().length == 2 && juego.sumOfPoints() == Constantes.MAX_PUNTOS) {
      return new ImpStrategiaSpare();
    }

    return new ImpStrategiaSimple();
  }
}
