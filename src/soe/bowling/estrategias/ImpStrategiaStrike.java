package soe.bowling.estrategias;

public class ImpStrategiaStrike extends ImpStrategiaChuza {

  private static final int strikeNextPointSize = 2;

  public ImpStrategiaStrike() {
    super(strikeNextPointSize);
  }
}
