package soe.bowling.estrategias;

import soe.bowling.errores.PuntosException;
import soe.bowling.Constantes;
import soe.bowling.entidades.Jugador;
import java.util.List;
import soe.bowling.IJuego;
import soe.bowling.IEstrategia;

public class ImpStrategiaSimple implements IEstrategia {

  public static final String exceedsMaxScoreTemplate =
      "The current sum of points [%d] in juego [%s] exceeds the max allowed (%s)";
  private final IJuego juegoOrganizer = IJuego.defaultjuegoOrganizer();

  @Override
  public void score(int juegoIndex, List<Jugador> juegos) throws PuntosException {
    int score =
        (juegoIndex == 1)
            ? Constantes.MIN_PUNTO
            : this.juegoOrganizer.getByIndex(juegoIndex - 1, juegos).getPunto();

    Jugador currentjuego = this.juegoOrganizer.getByIndex(juegoIndex, juegos);
    int currentSumOfPoints = currentjuego.sumOfPoints();

    if (juegoIndex != Constantes.JUEGOS_LENGTH
        && currentSumOfPoints > Constantes.MAX_PUNTOS) {
      throw new PuntosException(
          String.format(exceedsMaxScoreTemplate, currentSumOfPoints, juegoIndex, Constantes.MAX_PUNTOS));
    }

    score += currentjuego.sumOfPoints();
    currentjuego.setPunto(score);
  }
}
