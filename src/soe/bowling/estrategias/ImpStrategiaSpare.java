package soe.bowling.estrategias;

public class ImpStrategiaSpare extends ImpStrategiaChuza {

  private static final int spareNextPointSize = 1;

  public ImpStrategiaSpare() {
    super(spareNextPointSize);
  }
}
