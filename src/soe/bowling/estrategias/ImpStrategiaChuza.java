package soe.bowling.estrategias;

import soe.bowling.Constantes;
import soe.bowling.entidades.Jugador;
import java.util.List;
import soe.bowling.IJuego;
import soe.bowling.IEstrategia;

public class ImpStrategiaChuza implements IEstrategia {

  private final IJuego juegoOrganizer = IJuego.defaultjuegoOrganizer();
  private int nextPointSize;

  public ImpStrategiaChuza(int nextPointSize) {
    this.nextPointSize = nextPointSize;
  }

  @Override
  public void score(int juegoIndex, List<Jugador> juegos) {
    int score =
        (juegoIndex == 1)
            ? Constantes.MIN_PUNTO
            : this.juegoOrganizer.getByIndex(juegoIndex - 1, juegos).getPunto();

    Jugador currentjuego = this.juegoOrganizer.getByIndex(juegoIndex, juegos);
    score += currentjuego.sumOfPoints();

    int nextValueCounter = 0;
    int nextjuegoDistance = 1;

    while (nextValueCounter < this.nextPointSize) {
      final Jugador nextjuego = this.juegoOrganizer.getByIndex(juegoIndex + nextjuegoDistance, juegos);
      final int pointSize = nextjuego.getPuntos().length;
      nextjuegoDistance++;

      if (pointSize == 3) {
        if (nextValueCounter == 0) {
          score += (nextjuego.getPuntos()[0] + nextjuego.getPuntos()[1]);
          nextValueCounter += 2;
        }

        if (nextValueCounter == 1) {
          score += nextjuego.getPuntos()[0];
          nextValueCounter += 1;
        }

        continue;
      }

      if (nextValueCounter == 0) {
        score += this.nextPointSize == 1 ? nextjuego.getPuntos()[0] : nextjuego.sumOfPoints();
        nextValueCounter += pointSize;
        continue;
      }

      if (nextValueCounter == 1) {
        score += nextjuego.getPuntos()[0];
        nextValueCounter += pointSize;
      }
    }

    currentjuego.setPunto(score);
  }
}
