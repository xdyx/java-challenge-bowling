package soe.bowling;

import soe.bowling.errores.PuntosException;

public interface IPuntaje {

  int parseIntPoints(String inputScore) throws PuntosException;

  static IPuntaje defaultParser() {
    return new ImpPuntaje();
  }
}
