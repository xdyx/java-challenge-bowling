package soe.bowling;

public final class Constantes {

  private Constantes() { }

  public static final int ERROR_JUEGO = -1;
  public static final int MIN_PUNTO = 0;
  public static final int MAX_PUNTOS = 10;
  public static final int STRIKE_VALUE = 10;
  public static final int JUEGOS_LENGTH = 10;

  public static final String FALTA = "F";
  public static final String CHUZA = "X";
  public static final String SPARE = "/";
}
