package soe.bowling;

import soe.bowling.errores.PuntosException;
import java.util.Objects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ImpPuntaje implements IPuntaje {

  private static final Logger log = LogManager.getLogger();

  private final String warningTemplate = "Invalid Score found [%s], this invalidates the PlayerGame";

  @Override
  public int parseIntPoints(String inputScore)
      throws PuntosException {
    if (Objects.isNull(inputScore)) {
      validarError("null");
      return Constantes.MIN_PUNTO;
    }

    final String sanitizedInputScore = inputScore.trim().toUpperCase();
    if (Constantes.FALTA.equals(sanitizedInputScore)) {
      return Constantes.MIN_PUNTO;
    }

    try {
      final int score = Integer.parseInt(sanitizedInputScore);
      if (score < Constantes.MIN_PUNTO || score > Constantes.MAX_PUNTOS) {
        validarError(sanitizedInputScore);
      }
      return score;
    } catch (NumberFormatException exception) {
      validarError(sanitizedInputScore);
    }
    return Constantes.MIN_PUNTO;
  }

  private void validarError(String inputScore) throws PuntosException {
    log.info(String.format(warningTemplate, inputScore));
    throw new PuntosException(String.format(warningTemplate, inputScore));
  }
}
