package soe.bowling;

import soe.bowling.errores.PuntosException;
import soe.bowling.entidades.Jugador;
import soe.bowling.entidades.Turno;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import soe.bowling.estrategias.EstrategiasProvider;

public class ImpPuntajeCalculadora implements IPuntajeCalculadora {

  public static final Logger log = LogManager.getLogger();
  private static final String invalidNumberOfAttemptsTemplate = "Invalid Number of Attempts [%d]";

  private IJuego juegoOrganizer;

  public ImpPuntajeCalculadora() {
    this.juegoOrganizer = IJuego.defaultjuegoOrganizer();
  }

  public ImpPuntajeCalculadora(
      IJuego juegoOrganizer) {
    this.juegoOrganizer = juegoOrganizer;
  }

  @Override
  public void calcularPuntos(Turno playerGame) {
    try {
      this.juegoOrganizer.preparar(playerGame);
      this.calcular(playerGame.getjuegos());
      playerGame.setValido(Boolean.TRUE);
    } catch (PuntosException e) {
      playerGame.setValido(Boolean.FALSE);
      log.info(e.getMessage());
    }
  }

  private void validar(List<Jugador> juegos) throws PuntosException {
    if (juegos.size() != Constantes.JUEGOS_LENGTH) {
      throw new PuntosException(
          String.format(invalidNumberOfAttemptsTemplate, juegos.size()));
    }
  }

  private void calcular(List<Jugador> juegos) throws PuntosException {
    this.validar(juegos);
    for (Jugador f : juegos) {
      EstrategiasProvider.provideFor(f).score(f.getIndex(), juegos);
    }
  }
}
