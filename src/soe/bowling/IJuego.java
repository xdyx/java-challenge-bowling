package soe.bowling;

import soe.bowling.errores.JuegoException;
import soe.bowling.errores.PuntosException;
import soe.bowling.entidades.Jugador;
import soe.bowling.entidades.Turno;
import java.util.List;

public interface IJuego {

  List<Jugador> preparar(Turno playerGame) throws PuntosException;

  default Jugador getByIndex(int index, List<Jugador> juegos) {
    return juegos
        .stream()
        .filter(f -> f.getIndex() == index)
        .findFirst()
        .orElseThrow(JuegoException::new);
  }

  static IJuego defaultjuegoOrganizer() {
    return new ImpJuego();
  }
}
