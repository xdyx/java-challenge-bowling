package soe.bowling.entidades;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Turno {

  public static final Logger log = LogManager.getLogger();

  private final String nombre;
  private final List<String> puntos;

  private List<Jugador> juegos;
  private boolean valido;

  public Turno(String name) {
    this(name, new ArrayList<>());
  }

  public Turno(String name, List<String> inputScores) {
    this.nombre = name;
    this.puntos = inputScores;
    this.juegos = new ArrayList<>();
  }

  public List<String> getPuntos() {
    return this.puntos;
  }

  public String getNombre() {
    return this.nombre;
  }

  public List<Jugador> getjuegos() {
    return this.juegos;
  }

  public boolean isValido() {
    return this.valido;
  }

  public void setjuegos(List<Jugador> juegos) {
    this.juegos = juegos;
  }

  public void setValido(boolean valid) {
    this.valido = valid;
  }
}
