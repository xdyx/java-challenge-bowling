package soe.bowling.entidades;

import soe.bowling.errores.PuntosException;
import soe.bowling.Constantes;
import java.util.Arrays;
import soe.bowling.IPuntaje;

public class Jugador {

  private final IPuntaje scoreParser = IPuntaje.defaultParser();

  private int index;
  private String[] puntosStr;
  private int[] puntos;
  private int punto;

  public Jugador(int index, String[] stringPoints) {
    this.index = index;
    this.puntosStr = stringPoints;
    this.punto = Constantes.MIN_PUNTO;
    this.puntos =
        Arrays.stream(this.puntosStr)
            .mapToInt(p -> {
                  try {
                    return this.scoreParser.parseIntPoints(p);
                  } catch (PuntosException e) {
                    return Constantes.ERROR_JUEGO;
                  }
                })
            .filter(p -> p != Constantes.ERROR_JUEGO)
            .toArray();
  }

  public int[] getPuntos() {
    return this.puntos;
  }

  public String[] getPuntosStr() {
    return this.puntosStr;
  }

  public int getIndex() {
    return this.index;
  }

  public void setPunto(int punto) {
    this.punto = punto;
  }

  public int getPunto() {
    return this.punto;
  }

  public int sumOfPoints() {
    return Arrays.stream(this.puntosStr)
        .mapToInt(p -> {
              try {
                return this.scoreParser.parseIntPoints(p);
              } catch (PuntosException e) {
                return Constantes.ERROR_JUEGO;
              }
            })
        .sum();
  }

  public boolean isFirstjuego() {
    return this.index == 1;
  }
}
