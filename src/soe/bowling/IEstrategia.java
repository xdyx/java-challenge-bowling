package soe.bowling;

import soe.bowling.errores.PuntosException;
import soe.bowling.entidades.Jugador;
import java.util.List;

public interface IEstrategia {

  void score(int juegoIndex, List<Jugador> juegos)
      throws PuntosException;
}
