package soe.bowling;

import soe.bowling.errores.PuntosException;
import soe.bowling.entidades.Jugador;
import soe.bowling.entidades.Turno;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ImpJuego implements IJuego {

  private final IPuntaje scoreParser = IPuntaje.defaultParser();

  @Override
  public List<Jugador> preparar(Turno playerGame)
      throws PuntosException {
    playerGame.setjuegos(this.jugar(playerGame.getPuntos()));

    return playerGame.getjuegos();
  }

  private List<Jugador> jugar(List<String> inputPoints)
      throws PuntosException {
    Iterator<String> iterator = inputPoints.iterator();
    List<Jugador> juegos = new ArrayList<>();
    int index = 1;

    while (iterator.hasNext()) {
      String currentInput = iterator.next();
      int currentValue = this.scoreParser.parseIntPoints(currentInput);

      if (currentValue == Constantes.STRIKE_VALUE && index != Constantes.JUEGOS_LENGTH) {
        String[] points = {currentInput};
        juegos.add(new Jugador(index, points));
        index++;
        continue;
      }

      if (index != Constantes.JUEGOS_LENGTH) {
        String[] points = {currentInput, iterator.next()};
        juegos.add(new Jugador(index, points));
        index++;
      } else {
        String[] points = {currentInput, iterator.next(), iterator.next()};
        juegos.add(new Jugador(index, points));
        index++;
      }
    }

    return juegos;
  }
}
