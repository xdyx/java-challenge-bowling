package soe.bowling;

import soe.bowling.entidades.Turno;

public interface IPuntajeCalculadora {

  void calcularPuntos(Turno playerGame);

  static IPuntajeCalculadora defaultScoreCalculator() {
    return new ImpPuntajeCalculadora();
  }
}
