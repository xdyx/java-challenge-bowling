/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
import org.junit.Test;
import soe.bowling.IEstrategia;
import soe.bowling.IJuego;
import soe.bowling.entidades.Jugador;
import soe.bowling.errores.PuntosException;

/**
 *
 * @author gmm
 */
public class TestingEstrategias {
    
    private IJuego juego;
 
    
    @Test
    public   void test1Simple() throws PuntosException {
        int index = 3;
        Jugador previo;
        previo = new Jugador(2, new String[]{"7", "3"});
        previo.setPunto(39);

        List<Jugador> juegos = Arrays.asList(previo,
                new Jugador(index, new String[]{"9", "0"}));

        IEstrategia strategy = new soe.bowling.estrategias.ImpStrategiaSimple();
        strategy.score(index, juegos);
        int puntos=this.juego.getByIndex(index, juegos).getPunto();
        assertEquals(48, puntos);
    }
 
    
      @Test
    public   void test2Strike() throws PuntosException {
        int index = 1;
        

    List<Jugador> juegos = Arrays.asList(
        new Jugador(index, new String[] { "10" }),
        new Jugador(2, new String[] { "7", "3" }),
        new Jugador(3, new String[] { "9", "0" }));

        IEstrategia strategy = new soe.bowling.estrategias.ImpStrategiaStrike();
        strategy.score(index, juegos);
        int puntos=this.juego.getByIndex(index, juegos).getPunto();
        assertEquals(20, puntos);
        
         
 
    }
 
    
      @Test
    public   void test3Spare() throws PuntosException {
        int index = 1;
        
 List<Jugador> juegos = Arrays.asList(
        new Jugador(index, new String[] { "3", "7" }),
        new Jugador(2, new String[] { "6", "3" }),
        new Jugador(3, new String[] { "10" }));

        IEstrategia strategy = new soe.bowling.estrategias.ImpStrategiaSpare();
        strategy.score(index, juegos);
        int puntos=this.juego.getByIndex(index, juegos).getPunto();
        assertEquals(16, puntos);
          
 
    }
 
    @Before
    public void setUp() {
        this.juego = IJuego.defaultjuegoOrganizer();
    }
    
    
}
